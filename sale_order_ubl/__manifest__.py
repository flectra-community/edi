# © 2016-2017 Akretion (Alexis de Lattre <alexis.delattre@akretion.com>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Sale Order UBL",
    "version": "2.0.1.1.0",
    "category": "Sales",
    "license": "AGPL-3",
    "summary": "Embed UBL XML file inside the PDF quotation",
    "author": "Akretion,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/edi",
    "depends": ["sale", "base_ubl"],
    "installable": True,
}
