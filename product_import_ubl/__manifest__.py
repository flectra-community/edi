# Copyright 2022 Camptocamp SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Product UBL Import",
    "version": "2.0.1.1.1",
    "development_status": "Alpha",
    "license": "AGPL-3",
    "summary": "Import UBL XML catalogue files",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/edi",
    "depends": [
        "base_ubl",
        "product_import",
    ],
}
