# Copyright 2018-2021 Akretion France (http://www.akretion.com)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Account e-invoice Generate",
    "version": "2.0.1.0.0",
    "category": "Accounting & Finance",
    "license": "AGPL-3",
    "summary": "Technical module to generate PDF invoices with " "embedded XML file",
    "author": "Akretion,Onestein,Odoo Community Association (OCA)",
    "maintainers": ["alexis-via"],
    "website": "https://gitlab.com/flectra-community/edi",
    "depends": ["account"],
    "data": ["views/res_config_settings.xml"],
    "installable": True,
}
