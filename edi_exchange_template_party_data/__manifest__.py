# Copyright 2022 Camptocamp SA
# @author Simone Orsi <simahawk@gmail.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).
{
    "name": "EDI Exchange Template - Party data",
    "summary": """Glue module betweeb edi_exchange_template and edi_party_data""",
    "version": "2.0.1.0.1",
    "development_status": "Alpha",
    "license": "AGPL-3",
    "author": "Camptocamp,Odoo Community Association (OCA)",
    "maintainers": ["simahawk"],
    "website": "https://gitlab.com/flectra-community/edi",
    "depends": ["edi_exchange_template_oca", "edi_party_data_oca"],
    "auto_install": True,
}
