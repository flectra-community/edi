# Copyright 2017-2021 Akretion France (http://www.akretion.com/)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Py3o UBL Purchase Order",
    "version": "2.0.1.1.0",
    "category": "Purchases",
    "license": "AGPL-3",
    "summary": "Generate UBL purchase orders with Py3o reporting engine",
    "author": "Akretion,Odoo Community Association (OCA)",
    "maintainers": ["alexis-via"],
    "website": "https://gitlab.com/flectra-community/edi",
    "depends": ["purchase_order_ubl", "report_py3o_fusion_server"],
    "installable": True,
}
