# Copyright 2020 Creu Blanca
# Copyright 2022 Camptocamp
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "EDI WebService",
    "summary": """
        Defines webservice integration from EDI Exchange records""",
    "version": "2.0.1.4.0",
    "license": "AGPL-3",
    "development_status": "Beta",
    "author": "Creu Blanca, Camptocamp, Odoo Community Association (OCA)",
    "maintainers": ["etobella", "simahawk"],
    "website": "https://gitlab.com/flectra-community/edi",
    "depends": ["edi_oca", "webservice"],
    "data": ["views/edi_backend.xml", "security/ir.model.access.csv"],
    "demo": ["demo/edi_backend.xml"],
}
