# Flectra Community / edi

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[edi_account_invoice_import](edi_account_invoice_import/) | 2.0.1.1.0| Plug account_invoice_import into EDI machinery.
[account_invoice_download](account_invoice_download/) | 2.0.1.0.1| Auto-download supplier invoices and import them
[product_import](product_import/) | 2.0.1.1.1| Import product catalogues
[sale_order_customer_free_ref](sale_order_customer_free_ref/) | 2.0.1.0.0| Splits the Customer Reference on sale orders into two fields. An Id and a Free reference. The existing field is transformed into a computed one.
[edi_oca](edi_oca/) | 2.0.1.20.6|     Define backends, exchange types, exchange records,    basic automation and views for handling EDI exchanges.    
[account_invoice_import](account_invoice_import/) | 2.0.3.3.0| Import supplier invoices/refunds as PDF or XML files
[edi_pdf2data_oca](edi_pdf2data_oca/) | 2.0.1.0.1|         Module that allows to import data from a pdf
[edi_storage_oca](edi_storage_oca/) | 2.0.1.8.1|     Base module to allow exchanging files via storage backend (eg: SFTP).    
[edi_sale_order_import](edi_sale_order_import/) | 2.0.1.1.0| Plug sale_order_import into EDI machinery.
[edi_stock_oca](edi_stock_oca/) | 2.0.1.0.0|        Define EDI Configuration for Stock
[edi_account_oca](edi_account_oca/) | 2.0.1.1.1|         Define EDI Configuration for Account Moves
[account_invoice_download_scaleway](account_invoice_download_scaleway/) | 2.0.1.0.0| Get Scaleway Invoices via the API
[partner_identification_import](partner_identification_import/) | 2.0.1.0.1| Provides partner matching on extra ID
[edi_sale_order_import_ubl](edi_sale_order_import_ubl/) | 2.0.1.0.0| Plug sale_order_import_ubl into EDI machinery.
[pdf_helper](pdf_helper/) | 2.0.1.2.0| Provides helpers to work w/ PDFs
[edi_party_data_oca](edi_party_data_oca/) | 2.0.1.1.0|     Allow to configure and retrieve party information for EDI exchanges.    
[sale_order_import](sale_order_import/) | 2.0.1.5.0| Import RFQ or sale orders from files
[account_invoice_facturx_py3o](account_invoice_facturx_py3o/) | 2.0.1.0.0| Generate Factur-X invoices with Py3o reporting engine
[edi_purchase_oca](edi_purchase_oca/) | 2.0.1.0.0|         Define EDI Configuration for Purchase Orders
[edi_voxel_oca](edi_voxel_oca/) | 2.0.1.0.0| Base module for connecting with Voxel
[account_invoice_ubl_email_attachment](account_invoice_ubl_email_attachment/) | 2.0.1.0.0| Automatically adds the UBL file to the email.
[account_invoice_import_invoice2data](account_invoice_import_invoice2data/) | 2.0.2.1.1| Import supplier invoices using the invoice2data lib
[sale_order_packaging_import](sale_order_packaging_import/) | 2.0.1.1.0| Import the packaging on the sale order line
[account_invoice_ubl_peppol](account_invoice_ubl_peppol/) | 2.0.1.0.0| Generate invoices in PEPPOL 3.0 BIS dialect
[account_invoice_import_facturx](account_invoice_import_facturx/) | 2.0.1.0.0| Import Factur-X/ZUGFeRD supplier invoices/refunds
[account_invoice_import_simple_pdf](account_invoice_import_simple_pdf/) | 2.0.3.3.0| Import simple PDF vendor bills
[edi_webservice_oca](edi_webservice_oca/) | 2.0.1.4.0|         Defines webservice integration from EDI Exchange records
[edi_backend_partner_oca](edi_backend_partner_oca/) | 2.0.1.0.1| add the a partner field in EDI backend
[account_invoice_download_ovh](account_invoice_download_ovh/) | 2.0.1.1.0| Get OVH Invoice via the API
[edi_endpoint_oca](edi_endpoint_oca/) | 2.0.1.5.0|     Base module allowing configuration of custom endpoints for EDI framework.    
[account_invoice_import_ubl](account_invoice_import_ubl/) | 2.0.1.0.1| Import UBL XML supplier invoices/refunds
[edi_sale_order_import_ubl_endpoint](edi_sale_order_import_ubl_endpoint/) | 2.0.1.1.0| Provide a default endpoint to import SO in UBL format.
[account_invoice_ubl](account_invoice_ubl/) | 2.0.1.0.1| Generate UBL XML file for customer invoices/refunds
[sale_order_import_ubl](sale_order_import_ubl/) | 2.0.1.4.1| Import UBL XML sale order files
[edi_exchange_template_party_data](edi_exchange_template_party_data/) | 2.0.1.0.1| Glue module betweeb edi_exchange_template and edi_party_data
[base_ebill_payment_contract](base_ebill_payment_contract/) | 2.0.1.1.0|         Base for managing e-billing contracts
[base_ubl](base_ubl/) | 2.0.1.8.1| Base module for Universal Business Language (UBL)
[account_invoice_export_server_env](account_invoice_export_server_env/) | 2.0.1.0.0| Server environment for Account Invoice Export
[sale_order_import_ubl_line_customer_ref](sale_order_import_ubl_line_customer_ref/) | 2.0.1.0.2| Extract specific customer reference for each order line
[sale_order_ubl](sale_order_ubl/) | 2.0.1.1.0| Embed UBL XML file inside the PDF quotation
[base_facturx](base_facturx/) | 2.0.1.0.0| Base module for Factur-X/ZUGFeRD
[base_edi](base_edi/) | 2.0.1.0.0| Base module to aggregate EDI features.
[account_invoice_facturx](account_invoice_facturx/) | 2.0.1.1.0| Generate Factur-X/ZUGFeRD customer invoices
[product_import_ubl](product_import_ubl/) | 2.0.1.1.1| Import UBL XML catalogue files
[account_einvoice_generate](account_einvoice_generate/) | 2.0.1.0.0| Technical module to generate PDF invoices with embedded XML file
[purchase_order_ubl](purchase_order_ubl/) | 2.0.1.1.0| Embed UBL XML file inside the PDF purchase order
[base_business_document_import](base_business_document_import/) | 2.0.3.1.1| Provides technical tools to import sale orders or supplier invoices
[purchase_order_ubl_py3o](purchase_order_ubl_py3o/) | 2.0.1.1.0| Generate UBL purchase orders with Py3o reporting engine
[sale_order_import_ubl_customer_free_ref](sale_order_import_ubl_customer_free_ref/) | 2.0.1.1.0| Extract CustomerReference from sale UBL
[account_invoice_export](account_invoice_export/) | 2.0.1.2.3| Account Invoice Export
[edi_ubl_oca](edi_ubl_oca/) | 2.0.1.0.0| Define EDI backend type for UBL.
[purchase_stock_ubl](purchase_stock_ubl/) | 2.0.1.0.0| Glue module for Purchase Order UBL and Stock/Inventory
[edi_exchange_template_oca](edi_exchange_template_oca/) | 2.0.1.5.0| Allows definition of exchanges via templates.
[base_ubl_payment](base_ubl_payment/) | 2.0.1.0.0| Payment-related code for Universal Business Language (UBL)
[edi_xml_oca](edi_xml_oca/) | 2.0.1.1.0|         Base module for EDI exchange using XML files.    
[base_business_document_import_phone](base_business_document_import_phone/) | 2.0.1.0.0| Use phone numbers to match partners upon import of business documents


